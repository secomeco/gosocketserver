package main

import (
	"log"
	"time"
	"github.com/googollee/go-socket.io"
	"net/http"
	"fmt"
)

type Player struct {
	name string
	attack int
}
func (p Player) Update() {
	log.Println("player " + p.name +" updated!")
}

var players = make(map[string]Player)

func main() {
	//socketServer()
	gameLoop()
}

func gameLoop()  {
	tick := time.Tick(16 * time.Millisecond)

	for {
		select {
		case <-tick:

			fmt.Println(players)
			for player :=range players {
				fmt.Println(player)
			}

		}
	}
}

func socketServer() {

	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}
	server.On("connection", func(so socketio.Socket) {
		log.Println(so.Id())
		players[so.Id()] = Player{"seco", 2}

		log.Println("on connection")
		so.Join("chat")
		so.On("chat message", func(msg string) {
			log.Println("emit:", so.Emit("chat message", msg))
			so.BroadcastTo("chat", "chat message", msg)
		})
		so.On("disconnection", func() {
			log.Println("on disconnect")
		})
	})
	server.On("error", func(so socketio.Socket, err error) {
		log.Println("error:", err)
	})

	http.Handle("/socket.io/", server)
	http.Handle("/", http.FileServer(http.Dir("./asset")))
	log.Println("Serving at localhost:5000...")
	log.Fatal(http.ListenAndServe(":5000", nil))
}